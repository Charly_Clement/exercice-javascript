// fonction pour définir un cookie dans le navigateur
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

// fonction pour récupérer la valeur d'un cookie
function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

// clé de l'utilisateur si il existe déjà
var authKey = getCookie("authKey");

var currentUser;

if (authKey) { // si j'ai déjà un utilisateur

    // récupérer l'information de l'utilisateur courrant
    $.ajax({
        url: 'https://api.messenger.codecolliders.com/getUser',
        method: 'post',
        data: {
            authKey: authKey,
        },
    }).done(function (user) {
        currentUser = user;
        currentUser.authKey = authKey;
        getMessages();
        getUsers();
    });

} else { // si je n'ai pas encore d'utilisateur

    // création d'un nouvel utilisateur
    $.ajax('https://api.messenger.codecolliders.com/createUser').done(function (user) {
        currentUser = user;
        setCookie("authKey", user.authKey, 30);
        getMessages();
        getUsers();
    });
}

// id du dernier message récupéré
var lastId = 0;

// fonction permettant la récupération des messages
function getMessages() {

    $.ajax({
        url: 'https://api.messenger.codecolliders.com/getMessages',
        method: 'post',
        data: {
            authKey: currentUser.authKey,
            lastId: lastId,
        }
    }).done(function (messages) { // traitement et affichage des messages récupérés
        for (i = 0; i < messages.length; i++) {
            $('#messages').append('<p>' + messages[i].text + '</p>');
            lastId = messages[i].id;

            console.log(window.scrollTo());
            // on scroll automatiquement vers le bas de la page si on est déjà
            // en bas de la page
            if (window.scrollY + $(window).height() + 85 > $(document).height()) {
                window.scrollTo(0, $(document).height());
            }
        }

        // on recommence dans 1 seconde
        setTimeout(getMessages, 1000);
    });

}

// a qui envoyer le message ?
var to = 0;

// envoi d'un message avec la touche entrée du clavier
$('#newMessage').keypress(function (event) {
    if (event.keyCode === 13) {
        $.ajax({
            url: 'https://api.messenger.codecolliders.com/sendMessage',
            method: 'post',
            data: {
                authKey: currentUser.authKey,
                text: $(this).val(),
                to: to,
            }
        });

        $(this).val('');
    }
});


// récupération de la liste des personnes
function getUsers() {
    $.ajax('https://api.messenger.codecolliders.com/getUsers').done(function (users) {
        $('#users').html($('<p>tout le monde</p>').click(function () {
            to = 0;
        }));
        
        for (i = 0; i < users.length; i++) {
            // chaque utilisateur est cliquable, ce qui permet de lui envoyer un message privé
            $('#users').append($('<p data-id="' + users[i].id + '">' + users[i].username + '</p>').click(function () {
                to = $(this).attr('data-id');
            }));
        }

        setTimeout(getUsers, 5000);
    })
}
